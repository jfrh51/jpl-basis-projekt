wein(rotwein).
wein(weisswein).
schokolade(vollmilich).
isst(maria, vollmilch).
trinkt(maria, rotwein).
trinkt(peter, weisswein).
kennt(peter, maria).
kennt(maria,peter).
kennt(maria,klaus).
kenner(X):-trinkt(X,Y),wein(Y).
kenner(X):-isst(X,Y),schokolade(Y).
mag(maria,Y):-kennt(maria,Y),kenner(Y).