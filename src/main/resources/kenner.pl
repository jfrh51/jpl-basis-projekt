#needed to change the facts later
:- dynamic trinkt/2.
:- dynamic mag/2.
:- dynamic kenner/1.

wein(rotwein).
wein(weisswein).
schokolade(vollmilich).
isst(maria, vollmilch).
trinkt(maria, rotwein).
trinkt(peter, weisswein).
kennt(peter, maria).
kennt(maria,peter).
kennt(maria,klaus).
kenner(X):-isst(X,Y),schokolade(Y).
kenner(X):-trinkt(X,Y),wein(Y).
mag(maria,Y):-kennt(maria,Y),kenner(Y).
