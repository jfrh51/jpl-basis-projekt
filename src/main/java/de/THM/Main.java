package de.THM;

import org.jpl7.*;

import java.util.Map;


public class Main {

    static Atom maria = new Atom("maria");
    static Atom klaus = new Atom("klaus");
    static Atom peter = new Atom("peter");
    public static void main(String[] args) {
        System.out.println("JPL Version: " + JPL.version_string());

        Query q1 =
                new Query(
                        "consult",
                        new Term[] {new Atom("src/main/resources/kenner.pl")}
                );
        System.out.println( "consult " + (q1.hasSolution() ? "succeeded" : "failed"));

        Example1();
        Example2();
        Exe1();
        Sol1();
        Exe1();
        Sol2();
        Exe3();
        Sol3();
        Exe3();
    }

    static void Example1(){
        System.out.println( "\n----------- Beispiel 1 -----------");
        Variable Y = new Variable("Y");
        Query q =
                new Query(
                        "kennt",
                        new Term[] { maria,Y}
                );

        System.out.println( "Wen kennt Maria ? (?- mag(maria, X).)");

        java.util.Map<String,Term>[] solutions;

        solutions= q.allSolutions();
        for ( Map<String,Term> sol: solutions) {
            System.out.println(sol.get("Y"));
        }
    }

    static void Example2() {
        System.out.println( "\n----------- Beispiel 2 -----------");
        System.out.println("Wen kennt wen? (?- kennt(X,Y).)");
        Variable X = new Variable("X");
        Variable Y = new Variable("Y");

        Query q =
                new Query(
                        "kennt",
                        new Term[] {X,Y}
                );

        while ( q.hasNext() ){
            Map<String,Term> solution = q.next();
            System.out.println( "X = " + solution.get("X") + ", Y = " + solution.get("Y"));
        }
    }

    static void Exe1() {
        System.out.println( "\n----------- Aufgabe 1 -----------");
        System.out.println("Mag Peter Maria? (?- mag(maria,klaus).)");

        Query q = new Query("mag", new Term[] { peter,maria});

        if (q.hasSolution()) {
            System.out.println("Ja");
        } else {
            System.out.println("Nein");
        }

    }

    static void Exe3() {
        System.out.println( "\n----------- Aufgabe 3 -----------");
        System.out.println("Mag sich jeder selbst? (ex. ?- mag(maria,maria).)");

        Query q =
                new Query(
                        "mag(maria,maria)"
                );

        if ( q.allSolutions().length >= 1 ){
            System.out.println( "Ja");
        }else{
            System.out.println( "Nein");
        }
    }

    static void Sol1() {
        System.out.println( "\n----------- Loesung 1 -----------");
        System.out.println("Asset Rule: mag(X,Y)");
        (new Query("assert((mag(X,Y) :- kennt(X,Y),kenner(Y)))")).hasSolution();
    }

    static void Sol2(){
        System.out.println( "\n----------- Loesung 2 -----------");
        System.out.println("Jeder ist ein Kenner der Kaese isst und wein trinkt");
        (new Query("assert(kaese(camembert))")).hasSolution();
        (new Query("assert(kaese(appenzeller))")).hasSolution();
        (new Query("assert((kenner(X) :- isst(X, Y),kaese(Y),trinkt(X,rotwein)))")).hasSolution();
    }

    static void Sol3(){
        System.out.println( "\n----------- Loesung 3 -----------");
        System.out.println("Fakt: Jeder mag sich selbst ");
        (new Query("assert(mag(X,X))")).hasSolution();
    }


}
